# -*- coding: utf-8 -*-
"""
Created on Tue Dec 30 07:35:33 2014

@author: aaron
"""

#!/usr/bin/env python
#coding=utf-8
import os
from PIL import Image, ImageDraw
import cv
import cv2

def detect_object(image):
    '''检测图片，获取人脸在图片中的坐标'''
    grayscale = cv.CreateImage((image.width, image.height), 8, 1)
    cv.CvtColor(image, grayscale, cv.CV_BGR2GRAY)

    cascade = cv.Load("/usr/local/share/OpenCV/haarcascades/haarcascade_eye_tree_eyeglasses.xml")
    rect = cv.HaarDetectObjects(grayscale, cascade, cv.CreateMemStorage(), 1.1, 2,
        cv.CV_HAAR_DO_CANNY_PRUNING, (20,20))

    result = []
    for r in rect:
        result.append((r[0][0], r[0][1], r[0][0]+r[0][2], r[0][1]+r[0][3]))

    return result

def process(infile):
    '''在原图上框出头像并且截取每个头像到单独文件夹'''
    image = cv.LoadImage(infile);
    if image:
        faces = detect_object(image)

    im = Image.open(infile)
    path = os.path.abspath(infile)
    save_path = os.path.splitext(path)[0]+"_face"
    try:
        os.mkdir(save_path)
    except:
        pass
    if faces:
        draw = ImageDraw.Draw(im)
        count = 0
        for f in faces:
            count += 1
            draw.rectangle(f, outline=(255, 0, 0))
            a = im.crop(f)
            file_name = os.path.join(save_path,str(count)+".jpg")
     #       print file_name
            a.save(file_name)

        drow_save_path = os.path.join(save_path,"out.jpg")
        im.save(drow_save_path, "JPEG", quality=80)
        img = cv2.imread(drow_save_path)
        cv2.imshow('eye',img)
    else:
        print "Error: cannot detect faces on %s" % infile

if __name__ == "__main__":
    cv2.namedWindow('Video')
    cv2.namedWindow('eye')
    capture = cv2.VideoCapture(0)
    _, frame = capture.read()
    while frame is not None:
        cv2.imshow('Video', frame)
        key = cv2.waitKey(10)
        cv2.imwrite('screenshot.bmp', frame)
        process('screenshot.bmp')
        if key == ord('s'):     # 当按下"s"键时，将保存当前画面
            cv2.imwrite('screenshot.bmp', frame)
            process('screenshot.bmp')
        elif key == ord('q'):   # 当按下"q"键时，将退出循环
            cv2.destroyWindow('Video')
            break
        _, frame = capture.read()
    cv2.destroyWindow('Video')
    cv2.destroyWindow('eye')